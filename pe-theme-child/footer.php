<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package PE_Theme
 */

?>

<!-- -----Footer--Start----- -->
<footer id="colophon" class="site-footer">
    <div class="container">
    </div>
</footer>

<!-- -----Footer--End----- -->
</div>

<?php wp_footer(); ?>
<script type="text/javascript">
    jQuery(document).ready(function() {
        new fullpage('#fullpage', {});
    });
</script>
</body>

</html>