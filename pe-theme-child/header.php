<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package PE_Theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>



<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div id="page" class="site">
		<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e('Skip to content', 'pe-theme'); ?></a>
		<!-- -----Header--Start--- -->
		<header id="masthead" class="site-header">
			<div class="container">
				<div class="head-logo">
					<a href="<?php echo site_url(); ?>">
						<?php
						if (!empty(get_field('header_logo', 'option'))) { ?>
							<img src="<?php the_field('header_logo', 'option') ?>" class="banner-logo" alt="<?php echo get_bloginfo('name'); ?>" />
						<?php } ?>
					</a>
				</div>
				<!-- -----Navigation--Start--- -->
				<nav class="nav">
					<?php
					wp_nav_menu(array(
						'menu'           => 'Primary Menu',
						'theme_location' => 'primary',
						'menu_class'     => 'navigation-main'

					));
					?>
				</nav>
				<!-- -----Navigation--End--- -->
			</div>
		</header>
		<!-- -----Header--End----- -->