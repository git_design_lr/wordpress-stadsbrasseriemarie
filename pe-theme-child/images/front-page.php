<?php
/* 
* Front page
*/
get_header();

$banner_text = get_field('banner_text');
$menu_title = get_field( "menukaarten_title");
$nieuws_title = get_field( "nieuws_title");
$work_title = get_field( "werken_bij_brasserie_marie_title");


$footer_title = get_field( 'footer_section_title','option');
$address_line_1 = get_field( 'address_line_1','option' ); 
$address_line_2 = get_field( 'address_line_2','option' ); 
$phone = get_field( 'phone','option' ); 
$email = get_field( 'email','option' ); 
$site_url = get_field( 'site_url','option' ); 
$opening_hour_heading = get_field( 'opening_hour_heading','option' );
$opening_hour_time = get_field( 'opening_hour_time','option' );  
$parking_options_heading = get_field( 'parking_options_heading','option' );
$parking_address_line_1_link = get_field( 'parking_address_line_1_link','option' );
$parking_address_line_1 = get_field( 'parking_address_line_1','option' ); 
$parking_address_1_icon = get_field( 'parking_address_line_1_icon','option' ); 
$parking_address_line_2_link = get_field( 'parking_address_line_2_link','option' );
$parking_address_line_2 = get_field( 'parking_address_line_2','option' ); 
$parking_address_2_icon = get_field( 'parking_address_line_2_icon','option' ); 
$fisrt_button_link = get_field( 'first_button_link','option' );
$first_botton_text = get_field( 'first_botton_text','option' );
$second_button_link = get_field( 'second_button_link','option' );
$second_botton_text = get_field( 'second_botton_text','option' );

?>

<main id="site-main" class="main">
    <div id="fullpage">

        <!-- Header section start -->
        <section class="section home-hero" id="section0">
            <div class="banner-image-outet">

                <?php if( !empty( get_field('banner_image')) ){ ?>
                    <img src="<?php the_field('banner_image'); ?>" class="banner-img" alt="banner"/>
                <?php }    
               
                if( !empty(get_field('header_logo','option')) ){ ?>
                    <img src="<?php the_field('header_logo','option')?>" class="banner-logo" alt="banner" />
                <?php } ?>   

                <div class="rounded-logo font-ironclad">
                    <?php if( !empty($banner_text) ){ ?>
                        <p><?php echo $banner_text;?></p>
                    <?php } ?>
                </div>


                <dov class="hero-social">

                     <div class="social-icons">
                        <?php if( have_rows('social_list','option') && !empty(have_rows('social_list','option'))):
                        while( have_rows('social_list','option') ) : the_row();?>
                            <a href="<?php echo get_sub_field( "icon_link",'option');?>" class="social-link" target="_blank">
                                <img src="<?php echo get_sub_field('icon_image','option'); ?>" alt="Footer Social" />
                            </a>
                        <?php 
                            endwhile;
                            endif;
                        ?>
                    </div>
                </dov>
            </div>
            <div class="section-hr"></div>
            <div class="disc-area">
            <?php  if (!empty(get_the_content())) {
                        the_content();
                    }else{
                        echo "<h3 class='menu-no-content'>No content found</h3>";
             } ?>
            </div>
        </section>
        <!-- Header section end -->

        <!-- menu-section Strat -->
         <section class="section menu-section" id="section2">
            <div class="container">
                <h3 class="section-title"> <?php  if(!empty($menu_title)) { echo $menu_title; }?></h3>
               <div class="menu-slider-wrapper"> 
                <div class="arrows">
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
                <div class="swiper mySwiper menu-card-slider">
                    <div class="swiper-wrapper">
                        <?php
                            $menukaarten = get_field('menukaarten');
                            if( have_rows('menukaarten') ):
                                while( have_rows('menukaarten') ) : the_row();
                                    $menu_image = get_sub_field('menu_image'); ?>
                                        <div class="swiper-slide">
                                            <a href="<?php echo get_sub_field( "menu_pdf_link"); ?>" target="_blank">
                                                <img src="<?php echo  $menu_image; ?>" alt="menu-card-image" />
                                                <div class="menu-title-outer">
                                                    <h2 class="font-ironclad"><?php echo get_sub_field('menu_text'); ?></h2>
                                                </div>
                                            </a>
                                        </div>
                                    <?php
                                endwhile;
                            endif;
                        ?>
                    </div>
                </div>
               </div>
            </div>
            <div class="menu-bottom-hr">
                <div class="section-hr"></div>
            </div>
        </section>
        <!-- menu-section End --> 

        <!-- News Section start -->
        <section class="section news-section" id="section3">
            <div class="container">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/patten.svg" class="bg-pattern" alt="banner" />
                <h3 class="section-title"> <?php  if(!empty($nieuws_title)) { echo $nieuws_title; }?></h3>
                <?php
                    $nieuws = get_field('nieuws');
                    if( !empty($nieuws) ): 
                        foreach( $nieuws as $post ):
                            setup_postdata($post);
                        ?> 
                        <div class="news-outer">
                            <div class="news-text">
                                <h1><a href="<?php echo get_permalink($post->ID);?>"><?php the_title(); ?></a></h1>
                                <h3><?php  echo get_the_excerpt(); ?></h3>
                            </div>
                            <div class="news-img">
                                 <?php  the_post_thumbnail(); ?>    
                            </div>
                        </div>
                <?php endforeach; 
                 wp_reset_postdata(); 
                 endif; ?>
            </div>
            <div class="menu-bottom-hr">
                <div class="section-hr"></div>
            </div>
        </section>
        <!-- News Section End -->

        <!-- Gallary section  start-->
           
        <section class="section impression-section">
            <div class="container">
                <div class="impression-img-outer">
                <?php
                    $gallary_section = get_field('gallary_section');
                    if( have_rows('gallary_section') ):
                        while( have_rows('gallary_section') ) : the_row();
                        $gallary_image = get_sub_field('gallary_image'); ?>
                        <div class="impression-img">
                           <img src="<?php echo  $gallary_image; ?>" alt="banner" />
                        </div><?php
                        endwhile;
                    endif;
                 ?>
                </div>
            </div>
            <div class="menu-bottom-hr">
                <div class="section-hr"></div>
            </div>
        </section>      
        <!-- Gallary section end -->

        <!-- Work at section start -->
        <section class="section work-at-section">
            <div class="container">
                <h3 class="section-title"><?php  if(!empty($work_title)) { echo $work_title; }?></h3>
                <div class="work-gallery-line">
                    <div class="work-line">
                        <hr />
                    </div>
                    <div class="work-gallery">
                    <?php  $job_work = get_field('werken_bij_brasserie_marie');
                        if( !empty( $job_work )): 
                            foreach( $job_work as $post ):
                                setup_postdata($post);  ?>
                                <a href="<?php echo get_permalink($post->ID);?>">
                                <div class="gallery-img-box">
                                        <?php  the_post_thumbnail(); ?>
                                    <h3><?php the_title(); ?></h3>
                                </div>
                                </a><?php  
                            endforeach; 
                        wp_reset_postdata(); 
                        endif; ?>
                    </div>
                </div>
            </div>
            <div class="menu-bottom-hr">
                <div class="section-hr"></div>
            </div>
        </section>
        <!-- Work at section End -->  

        <!---To Book section start---->
        <section class="section to-book-section reserveren-sec">
            <div class="container">
                <?php if( have_rows('reserveren_settings') && !empty(have_rows('reserveren_settings'))): ?>
                    <?php while( have_rows('reserveren_settings') ): the_row(); ?>

                <h3 class="section-title"><?php echo get_sub_field('title');?></h3>
                <div class="disc-and-img-wrapper d-flex">
                    <div class="disc-and-img-outer d-flex">
                        <div class="disc-area">
                          <h1> <?php echo get_sub_field('content');?> </h1> 
                        </div>
                        <div class="book-logo-area">
                            <div class="book-logo">
                                <?php if( get_sub_field('image') ){ ?>
                                    <img src="<?php the_sub_field('image'); ?>" alt="Logo" />
                                <?php } ?>    
                            </div>
                            <div class="d-flex justify-content-center">
                                <?php 
                                $book_button = get_sub_field( "button_url");
                                $book_button_text = get_sub_field( "button_text");
                                if(!empty($book_button)){ ?>
                                <a href="<?php echo $book_button;?>" class="btn-white font-ironclad border-gold">
                                    <?php echo $book_button_text;?></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                 <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <div class="menu-bottom-hr">
                <div class="section-hr"></div>
            </div>
        </section>
        <!---To Book section end---->

        <!---Gift Card section Start---->
        <!-- <section class="section gift-card-section">
            <div class="container">
                 <?php if( have_rows('cadeaubonnen_settings') ): ?>
                    <?php while( have_rows('cadeaubonnen_settings') ): the_row(); ?>
                <h3 class="section-title"><?php echo get_sub_field('title');?></h3>
                <div class="gift-card-outer">
                    <div class="gif-img">
                        <?php if( get_sub_field('background_image') ){ ?>
                                    <img src="<?php the_sub_field('background_image'); ?>" class="bg-pattern" alt="banner"  />
                        <?php } if( get_sub_field('image') ){ ?>
                                    <img src="<?php the_sub_field('image'); ?>" class="middle-img" alt="banner"/>
                        <?php } ?>

                    </div>
                    <div class="disc-area">
                         <h1><?php echo get_sub_field('content');?></h1> 
                       
                        <div class="d-flex justify-content-right">
                            <?php 
                                $gift_button = get_sub_field( "button_url");
                                $gift_button_text = get_sub_field( "button_text");
                                if(!empty($gift_button)){ ?>
                                <a href="<?php echo $gift_button;?>" class="btn-white font-ironclad border-gold">
                                    <?php echo $gift_button_text;?></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                 <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <div class="menu-bottom-hr">
                <div class="section-hr"></div>
            </div>
        </section> -->
        <!---Gift Card section end---->

        <!-- Green angle section start -->
          <!-- <section class="section to-book-section green-angel-section">
            <div class="container">
                <?php if( have_rows('groene_engel_settings') ): 
                     while( have_rows('groene_engel_settings') ): the_row(); ?>
                <h3 class="section-title"><?php echo get_sub_field('title');?></h3>
                <div class="disc-and-img-wrapper d-flex">
                    <div class="disc-and-img-outer d-flex">
                        <div class="disc-area">
                             <?php echo get_sub_field('content');?>
                        </div>
                        <div class="angel-logo-area">
                            <div class="book-logo">
                               <?php if( get_sub_field('image') ){ ?>
                                    <img src="<?php the_sub_field('image'); ?>" alt="Logo" />
                                <?php } ?> 
                            </div>
                            <div class="d-flex justify-content-center">
                                <?php 
                                $green_angle_button = get_sub_field( "button_url");
                                $green_angle_button_text = get_sub_field( "button_text");
                                if(!empty($green_angle_button)){ ?>
                                <a href="<?php echo $green_angle_button;?>" class="btn-white font-ironclad border-gold">
                                    <?php echo $green_angle_button_text;?></a>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                 <?php endwhile; 
            endif; ?>
            </div>
            <div class="menu-bottom-hr">
                <div class="section-hr"></div>
            </div>
        </section> -->
        <!-- green angle section end -->




        <!-- Footer section start -->
        <section class="section footer-section">
            <footer>
                <div class="container-grid">
                    <div class="footer-logo">
                       <?php if( !empty(get_field('footer_logo','option')) ){ ?>
                            <img src="<?php the_field('footer_logo','option'); ?>" alt="Footer Logo" />
                        <?php } ?>    
                    </div>

                    <div class="footer-address">
                        <h2 class="font-ironclad"><?php  
                            if(!empty($footer_title)) { 
                                echo $footer_title; }
                            ?> 
                        </h2>
                        <ul>
                           <?php 
                                if(!empty($address_line_1)){ ?>
                                    <li>
                                        <?php echo $address_line_1; ?>
                                    </li>
                            <?php } 

                                if(!empty($address_line_2)){ ?>
                                    <li>
                                       <?php echo $address_line_2; ?>
                                    </li>
                            <?php }

                                if(!empty($phone)){
                                    $phone_link = str_replace('-', '', $phone);
                                 ?>
                                    <li>Phone : 
                                        <a href="tel:<?php echo $phone_link; ?>"><?php echo $phone; ?></a>
                                    </li>
                            <?php } 
                                
                                if(!empty($email)){ ?>
                                    <li>
                                        <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
                                    </li>
                            <?php } 

                                if(!empty($site_url)){ ?>
                                    <li>
                                        <a href="https://<?php echo $site_url; ?>"><?php echo $site_url; ?></a>
                                    </li>
                            <?php } ?>

                        </ul>

                        <ul>
                            <?php 
                                if(!empty($opening_hour_heading)){ ?>
                                    <li>
                                        <?php echo $opening_hour_heading; ?>
                                    </li>
                            <?php } 

                                if(!empty($opening_hour_time)){ ?>
                                    <li>
                                        <?php echo $opening_hour_time; ?>
                                    </li>
                            <?php } ?>
                        </ul>
                        <ul>
                             <?php  
                                if(!empty($parking_options_heading)){ ?>
                                    <li>
                                        <?php echo $parking_options_heading; ?>
                                    </li>
                            <?php }
                           
                            if(!empty($parking_address_line_1)){ ?>
                                    <li>
                                        <a href="<?php echo $parking_address_line_1_link;?>" target="_blank">  
                                            <?php  echo $parking_address_line_1; ?>
                                            <img src="<?php echo $parking_address_1_icon; ?>" />
                                        </a> 
                                    </li>
                                
                            <?php }
                           
                            if(!empty($parking_address_line_2)){ ?>
                               
                                    <li>
                                        <a href="<?php echo  $parking_address_line_2_link;?>" target="_blank">  
                                            <?php  echo $parking_address_line_2; ?>
                                            <img src="<?php echo $parking_address_2_icon; ?>" />
                                        </a>  
                                    </li>
                                 
                            <?php } ?> 
                        </ul>
                    </div>

                    <div class="footer-button">
                        <div class="footer-btn">
                            <?php if(!empty($fisrt_button_link)){ ?>
                                 <a href="<?php echo $fisrt_button_link; ?>" class="btn-white font-ironclad"> 
                                    <?php echo $first_botton_text; ?>
                                </a>
                            <?php } if(!empty($second_button_link)){ ?>
                                    <a href="<?php echo $second_button_link; ?>" class="btn-white font-ironclad">
                                        <?php echo $second_botton_text; ?>
                                    </a>
                            <?php } ?> 
                        </div>
                    </div>
                    <div class="social-icons">
                        <?php if( have_rows('social_list','option') ):
                        while( have_rows('social_list','option') ) : the_row();?>
                            <a href="<?php echo get_sub_field( 'icon_link','option');?>" class="social-link" target="_blank">
                                <img src="<?php echo get_sub_field('icon_image','option');?>" alt="Footer Social" />
                            </a>
                        <?php 
                        endwhile;
                        endif;
                        ?>
                    </div>
                </div>
            </footer>
        </section>
        <!-- Footer Section end -->

    </div>


</main>


<?php
get_footer();
