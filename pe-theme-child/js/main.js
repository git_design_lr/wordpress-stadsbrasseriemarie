jQuery( document ).ready(function() {
    console.log( "Ready 🚀" );
    jQuery('body').addClass('body-ready');
     
    jQuery("#search-icon").click(function() {  
        jQuery('.woocommerce-navigation').addClass("search-bar-active");   
        jQuery('body').addClass("h-search-active");    
    });
    jQuery("#close-search-icon").click(function() {  
        jQuery('.woocommerce-navigation').removeClass("search-bar-active");
        jQuery('body').removeClass("h-search-active");     
    });

    // var archiveUsp = jQuery('.archive-usp');
    // archiveUsp.insertAfter('ul.products li:nth-child(7)');

    jQuery( ".accordion" ).accordion({
      collapsible: true,
      active: false,
      heightStyle: 'content',
    });  
    
    var timeout;
    jQuery( function( $ ) {
        jQuery('.woocommerce').on('change', 'input.qty', function(){
            if ( timeout !== undefined ) {
                clearTimeout( timeout );
            }
            timeout = setTimeout(function() {
                jQuery("[name='update_cart']").trigger("click");
            }, 300 ); 

        });
    } );

    const homeSwiper = new Swiper('.swiper.home-slider', {       
        slideVisibleClass: "visible-slide",
        slidesPerView: 4,
        spaceBetween: 26,
        freeMode: false,        
        grabCursor: false,
        centeredSlides: false,
        loop: true, 
        speed: 900,          
        autoplay: {
          delay: 2500,
          disableOnInteraction: false,
        },          
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },      
        breakpoints: {
            835: {
                spaceBetween: 26,
            },
            1025: {
                spaceBetween: 26,
            }
        }  
    }); 

    jQuery('.shop_usp').readmore({
      speed: 75,
      moreLink: '<a href="#" class="text-link">Meer lezen</a>',
      lessLink: '<a href="#" class="text-link">Lees minder</a>',
      collapsedHeight: 135,
      blockCSS: 'display: block; width: 100%; overflow: hidden;'
    });

    jQuery('#repair_option_field').click(function() {
        jQuery('#repair_option_field').addClass('active-check');   
    });

    // filter
    jQuery('#sort-button').click(function() {
        jQuery('.filter-modal').toggleClass('active-modal-sort');
        jQuery('.filter-modal').removeClass('active-modal-filter');     
    });
    jQuery('#filter-button').click(function() {
        jQuery('.filter-modal').toggleClass('active-modal-filter');
        jQuery('.filter-modal').removeClass('active-modal-sort');     
    });
    jQuery('body').keydown(function(e){
        if(e.keyCode == 27) {
            jQuery('.filter-modal').removeClass('active-modal-sort'); 
             jQuery('.filter-modal').removeClass('active-modal-filter');
        }
    });
    jQuery(document).on('click', function(e) {
        if (jQuery(e.target).is('.filter-modal, #filter-button, #sort-button, .filter-modal li, .filter-modal select, .filter-modal input, .filter-modal h4, .filter-modal meta-slider, .filter-modal ul, .noUi-handle, .noUi-base, .noUi-connect') === false) {
            jQuery('.filter-modal').removeClass('active-modal-sort');
            jQuery('.filter-modal').removeClass('active-modal-filter');
        }
    });

});   

jQuery(document).ready(function menuToggleOne() {
    jQuery('#wc1').hover(function () {
        jQuery('body, #mega-1').addClass('active-mega', 0);
        jQuery('#mega-2, #mega-3, #mega-4, #mega-5').removeClass('active-mega', 0);
    });
    jQuery('#wc2').hover(function () {
        jQuery('body, #mega-2').addClass('active-mega', 0);
        jQuery('#mega-1, #mega-3, #mega-4, #mega-5').removeClass('active-mega', 0);
    });
    jQuery('#wc3').hover(function () {
        jQuery('body, #mega-3').addClass('active-mega', 0);
        jQuery('#mega-1, #mega-2, #mega-4, #mega-5').removeClass('active-mega', 0);
    });
    jQuery('#wc4').hover(function () {
        jQuery('body, #mega-4').addClass('active-mega', 0);
        jQuery('#mega-1, #mega-2, #mega-3, #mega-5').removeClass('active-mega', 0);
    });
    jQuery('#wc5').hover(function () {
        jQuery('body, #mega-5').addClass('active-mega', 0);
        jQuery('#mega-1, #mega-2, #mega-3, #mega-4').removeClass('active-mega', 0);
    });
    jQuery('.header-overlay, main, #site-content, .top-part-header, .site-branding, .site-footer, .woo-icons, .main-navigation').hover(function () {
        jQuery('body, #mega-1, #mega-2, #mega-3, #mega-4, #mega-5').removeClass('active-mega', 0);
    });
});







jQuery(window).on("scroll", function () {
    var sticky = jQuery('.site-header'),
    scroll = jQuery(window).scrollTop();
    if (scroll >= 50) sticky.addClass('fixed');
    else sticky.removeClass('fixed');
});