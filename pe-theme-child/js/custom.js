jQuery(document).ready(function () {

  var swiper = new Swiper(".mySwiper", {
    slidesPerView: 4,
    spaceBetween: 0,
        autoplay: {
      delay: 2500,
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  });

  var swiper = new Swiper(".menuSwiper", {
    slidesPerView: 4,
    spaceBetween: 0,
    autoplay: {
      delay: 2500,
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  });

});
