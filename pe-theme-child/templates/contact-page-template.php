<?php

/* Template Name: Contact Page Template */ 

get_header(); ?>
	<section class="section contact-section inner-section">
            <div class="container">
                <h3 class="section-title"><?php the_title(); ?></h3>

                <div class="contact-form-outer">
                    <?php the_content(); ?>
                    <?php
                    echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]');
                    ?>
                </div>
            </div>
            <div class="menu-bottom-hr">
                <div class="section-hr"></div>
            </div>
        </section>

<?php get_footer('inner'); 