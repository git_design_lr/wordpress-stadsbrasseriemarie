<?php

/* Template Name: Gift Card Page Template */ 

get_header(); ?>

 <section class="section gift-card-section inner-section">
            <div class="container">
                 
                <h3 class="section-title"><?php the_title(); ?></h3>
                <div class="gift-card-outer">
                    <div class="gif-img">
                        <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID()); ?>   
                        <?php if( !empty( get_field('post_background_image')) ){ ?>
                            <img src="<?php the_field('post_background_image'); ?>" class="bg-pattern" alt="banner"/>
                        <?php } ?>          
                        <img src="<?php if(!empty($featured_img_url)){echo $featured_img_url;}  ?>" class="middle-img" alt="banner"/>
                     </div>
                    <div class="disc-area">
                        <?php 
                            if (!empty(get_the_content())) {
                                the_content();
                            }else{
                               echo "<h3 class='menu-no-content'>No content found</h3>";
                            }
                        ?>
                        <div class="d-flex justify-content-right">
                            <?php 
                                $book_button = get_field( 'button_link' );
                                $book_button_text = get_field( 'button_text' );
                                if(!empty($book_button)){ ?>
                                <a href="<?php echo $book_button;?>" class="btn-white font-ironclad border-gold">
                                    <?php echo $book_button_text;?></a>
                            <?php } ?>    
                        </div>
                    </div>
                </div>                 
            </div>
            <div class="menu-bottom-hr">
                <div class="section-hr"></div>
            </div>
        </section>
<?php
get_footer('inner');
