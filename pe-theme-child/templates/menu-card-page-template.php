<?php

/* Template Name: Menu Card Page Template */ 

get_header();  ?>

<section class="section menu-section inner-section" id="section2">
    <div class="container">
        <h3 class="section-title"> <?php the_title(); ?></h3>
       <div class="menu-slider-wrapper"> 
        <div class="arrows">
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>
        <div class="swiper menuSwiper menu-card-slider">
            <div class="swiper-wrapper">
                <?php
                    $menukaarten = get_field('menukaarten');
                    if( have_rows('menukaarten') ):
                        while( have_rows('menukaarten') ) : the_row();
                            $menu_image = get_sub_field('menu_image');
                            ?>
                            <div class="swiper-slide">
                                <a href="<?php echo get_sub_field( "menu_link_pdf"); ?>" target="_blank">
                                    <img src="<?php echo  $menu_image; ?>" alt="<?php echo 'menu-card-image'; ?>" />
                                    <div class="menu-title-outer">
                                        <h2 class="font-ironclad"><?php echo get_sub_field('menu_text'); ?></h2>
                                    </div>
                                </a>
                            </div>
                            <?php
                        endwhile;
                    endif;
                ?>
            </div>
        </div>

       </div>
    </div>
    <div class="menu-bottom-hr">
        <div class="section-hr"></div>
    </div>
</section>

<?php get_footer('inner');?>