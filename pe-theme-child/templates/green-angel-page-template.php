<?php

/* Template Name: Green Angel Page Template */ 

get_header(); ?>

  	<section class="section to-book-section green-angel-section inner-section">
            <div class="container">
                <h3 class="section-title"><?php  the_title();?></h3>
                <div class="disc-and-img-wrapper d-flex">
                    <div class="disc-and-img-outer d-flex">
                        <div class="disc-area">
                        <?php 
                            if (!empty(get_the_content())) {
                                the_content();
                            }else{
                               echo "<h3 class='menu-no-content'>No content found</h3>";
                            }
                        ?>
                        </div>
                        <div class="angel-logo-area">
                            <div class="book-logo">
                                    <?php  if ( has_post_thumbnail() ) { the_post_thumbnail();} ?>
                            </div>
                            <div class="d-flex justify-content-center">
                            <?php 
                                $green_angle_button = get_field( "button_link");
                                $green_angle_button_text = get_field( "button_text");
                                if(!empty($green_angle_button)){ ?>
                                <a target="_blank" href="<?php echo $green_angle_button;?>" class="btn-white font-ironclad border-gold">
                                    <?php echo $green_angle_button_text;?></a>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                 
            </div>
            <div class="menu-bottom-hr">
                <div class="section-hr"></div>
            </div>
    </section>


<?php get_footer('inner');
