<?php

/* Template Name: Work at Page Template */

get_header(); ?>

<section class="section work-at-section inner-section">
    <div class="container">
        <h3 class="section-title"><?php the_title(); ?></h3>
        <?php
        $job_work = get_posts([
            'post_type' => 'vacature',
            'post_status' => 'publish',
            'numberposts' => -1
        ]);
        $count = 0;
        ?>
        <div class="work-gallery-line">
            <div class="work-line">
                <hr />
            </div>
            <div class="work-gallery">
                <?php
                if (!empty($job_work)) :
                    foreach ($job_work as $post) :
                        setup_postdata($post);
                ?>
                    <a href="<?php echo get_permalink($post->ID); ?>">
                        <div class="gallery-img-box">
                            <?php  if ( has_post_thumbnail() ) { the_post_thumbnail();} ?>
                            <h3><?php the_title(); ?></h3>
                        </div>
                    </a>
                <?php 
                $count++;
                if ($count % 3 == 0 && $count < count($job_work)) {
                  echo '</div></div><div class="work-gallery-line">
                  <div class="work-line">
                      <hr />
                  </div><div class="work-gallery">';
                }
                endforeach;
                    wp_reset_postdata();
                endif; ?>
            </div>
        </div>

    </div>
    <div class="menu-bottom-hr">
        <div class="section-hr"></div>
    </div>
</section>
<?php

get_footer('inner');

?>