# Stadsbrasseriemarie - Restaurant (WP)

At our nice neighbor Cultuurpodium Groene Engel you can go for a versatile and varied program for young and old in the fields of theatre, film, music, dance and lectures/debate.
## 🚀 Technology Used
- Design
  - Figma to Wordpress
  - SCSS with Variables
  - Mobile Responsive
- CMS
  - Wordpress
- Database
  - MySQL
- Web servers
  - Apache HTTP Server
- JavaScript libraries
  - Swiper
  - jQuery
## 🔗 Live Link
https://stadsbrasseriemarie.nl/

## 🖼️ Screenshots

![App Screenshot](https://i.imgur.com/3QV40Mr.jpg)

![App Screenshot](https://i.imgur.com/S7l8Xva.png)

